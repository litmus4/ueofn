// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameplayTagContainer.h"
#include "GameplayTagParentActor.generated.h"

class UBoxComponent;

UCLASS()
class UEOFN_API AGameplayTagParentActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGameplayTagParentActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UBoxComponent* m_pBoxComp;

	UFUNCTION()
	void OnBoxOverlapped(UPrimitiveComponent* pOverlappedComponent, AActor* pOtherActor,
		UPrimitiveComponent* pOtherComp, int32 iOtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	bool IsGameplayTagChildOf(const FGameplayTag& CheckedTag, const FGameplayTag& ParentTag);

	void RunSelfBundle(int32 iCycle, const FGameplayTag& CheckedTag, const FGameplayTag& ParentTag);
	void RunMatchesBundle(int32 iCycle, const FGameplayTag& CheckedTag, const FGameplayTag& ParentTag);
};
