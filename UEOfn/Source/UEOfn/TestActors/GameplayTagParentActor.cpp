// Fill out your copyright notice in the Description page of Project Settings.


#include "GameplayTagParentActor.h"
#include "Components/BoxComponent.h"

// Sets default values
AGameplayTagParentActor::AGameplayTagParentActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	m_pBoxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComp"));
	m_pBoxComp->OnComponentBeginOverlap.AddDynamic(this, &AGameplayTagParentActor::OnBoxOverlapped);
	RootComponent = m_pBoxComp;
}

// Called when the game starts or when spawned
void AGameplayTagParentActor::BeginPlay()
{
	Super::BeginPlay();
	
}

void AGameplayTagParentActor::OnBoxOverlapped(UPrimitiveComponent* pOverlappedComponent, AActor* pOtherActor,
	UPrimitiveComponent* pOtherComp, int32 iOtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	static const FGameplayTag CheckTag = FGameplayTag::RequestGameplayTag(TEXT("MatchesTotalRoot.Second.CategoryMain.TagMain"));
	static const FGameplayTag MatchedRootTag = FGameplayTag::RequestGameplayTag(TEXT("MatchesTotalRoot"));
	static const FGameplayTag MatchedParentTag = FGameplayTag::RequestGameplayTag(TEXT("MatchesTotalRoot.Second.CategoryMain"));
	static const FGameplayTag NotMatchedTag = FGameplayTag::RequestGameplayTag(TEXT("NotMatchesTotalRoot.Second.Third"));

	int32 iCycle = 1000000;

	//结论：引擎自带的非Exact函数明显比直接拆字符串效率高不少，我错了
	RunSelfBundle(iCycle, CheckTag, MatchedRootTag);
	RunMatchesBundle(iCycle, CheckTag, MatchedRootTag);

	RunSelfBundle(iCycle, CheckTag, MatchedParentTag);
	RunMatchesBundle(iCycle, CheckTag, MatchedParentTag);

	RunSelfBundle(iCycle, CheckTag, NotMatchedTag);
	RunMatchesBundle(iCycle, CheckTag, NotMatchedTag);
}

// Called every frame
void AGameplayTagParentActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool AGameplayTagParentActor::IsGameplayTagChildOf(const FGameplayTag& CheckedTag, const FGameplayTag& ParentTag)
{
	if (!CheckedTag.IsValid() || !ParentTag.IsValid() || CheckedTag == ParentTag)
		return false;

	FString&& CheckedStr = CheckedTag.ToString();
	FString&& ParentStr = ParentTag.ToString();
	if (CheckedStr.Find(ParentStr, ESearchCase::Type::CaseSensitive) == 0)
		return CheckedStr.RightChop(ParentStr.Len())[0] == '.';
	return false;
}

void AGameplayTagParentActor::RunSelfBundle(int32 iCycle, const FGameplayTag& CheckedTag, const FGameplayTag& ParentTag)
{
	for (int32 i = 0; i < iCycle; ++i)
	{
		bool&& bRet = IsGameplayTagChildOf(CheckedTag, ParentTag);
		int iii = 0;
	}
}

void AGameplayTagParentActor::RunMatchesBundle(int32 iCycle, const FGameplayTag& CheckedTag, const FGameplayTag& ParentTag)
{
	for (int32 i = 0; i < iCycle; ++i)
	{
		bool&& bRet = CheckedTag.MatchesTag(ParentTag);
		int iii = 0;
	}
}