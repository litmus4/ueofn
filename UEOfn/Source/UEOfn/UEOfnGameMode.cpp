// Copyright Epic Games, Inc. All Rights Reserved.

#include "UEOfnGameMode.h"
#include "UEOfnCharacter.h"
#include "UObject/ConstructorHelpers.h"

AUEOfnGameMode::AUEOfnGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
